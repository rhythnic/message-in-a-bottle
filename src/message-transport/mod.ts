import { FirstHandlerArg } from "../mod.ts";
import { EventEmitter } from "../deps.ts";

interface CurrentRequest {
  resolve(result: unknown): void;
  reject(err: Error): void;
}

export type Message = [string, number, string, unknown[]];

interface Request {
  type: string;
  correlationId?: number;
  key: string;
  args: unknown[];
}

interface MessageTransportOptions {
  timeout?: number;
}

export abstract class MessageTransport extends EventEmitter {
  private outstandingRequests: Map<number, CurrentRequest>;
  private $: FirstHandlerArg | null = null;
  private opts: MessageTransportOptions;
  constructor(opts: MessageTransportOptions = {}) {
    super();
    this.outstandingRequests = new Map();
    this.opts = Object.assign({ timeout: 5000 }, opts);
  }
  initialize($: FirstHandlerArg) {
    this.$ = $;
  }
  defaultHandler(
    $: FirstHandlerArg,
    cmd: string,
    ...args: unknown[]
  ): Promise<unknown> {
    return new Promise((resolve, reject) => {
      const id = setTimeout(() => {
        this.outstandingRequests.delete(id);
        reject(new Error("Command timed out"));
      }, this.opts.timeout);
      this.outstandingRequests.set(id, { resolve, reject });
      this.emit("send", ["do-request", id, cmd, args]);
    });
  }
  sendEvent(topic: string, ...args: unknown[]): void {
    this.emit("send", ["emit-request", 0, topic, args]);
  }
  receive(data: Message): null | Request {
    let [type, id, cmdOrTopic, args] = data;
    switch (type) {
      case "do-success":
      case "do-failure":
        this.doResponse(type, id, args[0]);
        return null;
      case "do-request":
        return { type: "command", correlationId: id, key: cmdOrTopic, args };
      case "emit-request":
        return { type: "event", key: cmdOrTopic, args };
      default:
        return null;
    }
  }
  private respondWithError(req: Request, err: Error) {
    this.emit("send", ["do-failure", req.correlationId, "", [err.message]]);
  }
  private respondWithResult(req: Request, result: unknown) {
    this.emit("send", ["do-success", req.correlationId, "", [result]]);
  }
  respond(req: Request, result: unknown): void {
    if (result instanceof Promise) {
      result.then(
        (x) => this.respondWithResult(req, x),
        (err) => this.respondWithError(req, err),
      );
    } else if (result instanceof Error) {
      this.respondWithError(req, result);
    } else {
      this.respondWithResult(req, result);
    }
  }
  execute(msg: Message): void {
    const req = this.receive(msg);
    if (!req) return;
    if (req.type === "event") {
      (this.$ as FirstHandlerArg).emit(req.key, ...req.args);
      return;
    }
    if (req.type === "command") {
      try {
        this.respond(req, (this.$ as FirstHandlerArg).do(req.key, ...req.args));
      } catch (err) {
        this.respond(req, err);
      }
    }
  }
  private doResponse(type: string, id: number, data: unknown): void {
    try {
      clearTimeout(id);
      const request = this.outstandingRequests.get(id);
      this.outstandingRequests.delete(id);
      if (!request) return;
      if (type === "do-success") request.resolve(data);
      else request.reject(new Error(data as string));
    } catch (error) {
      (this.$ as FirstHandlerArg).emit("error", error);
    }
  }
}
