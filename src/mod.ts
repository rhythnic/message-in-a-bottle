import { EventEmitter } from "./deps.ts";

interface CommandContext {
  $catch?: string;
  $logArguments?: boolean;
  [propName: string]: unknown;
}

interface CommandObject extends CommandContext {
  $cmd: string;
}

export type FirstHandlerArg = CommandContext & Dispatcher;

interface Plugin {
  [cmd: string]: ($: FirstHandlerArg, ...args: unknown[]) => unknown;
}

export interface CommandStackItem {
  cmd: string;
  args: unknown[];
}

export class CommandError extends Error {
  commandStack: CommandStackItem[];
  constructor(public source: Error) {
    super(source.message);
    this.source = source;
    this.commandStack = [];
  }
}

export class Dispatcher extends EventEmitter {
  private plugins: Map<string, Plugin>;
  private globalCtx: CommandContext;
  private cmdCtx: Map<string, CommandContext>;
  constructor() {
    super();
    this.globalCtx = {};
    this.cmdCtx = new Map();
    this.plugins = new Map();
  }
  mergeContext(ctx: CommandContext, cmd?: string): Dispatcher {
    const conflict = Object.keys(ctx).find((x) => this.hasOwnProperty(x));
    if (conflict) {
      let msg = `mergeContext key ${conflict} conflicts with Dispatcher API`;
      throw new Error(msg);
    }
    if (cmd) {
      this.cmdCtx.set(cmd, Object.assign({}, this.cmdCtx.get(cmd), ctx));
    } else {
      Object.assign(this.globalCtx, ctx);
    }
    return this;
  }
  private buildCtx(
    ...ctx: (CommandContext | CommandObject | void)[]
  ): FirstHandlerArg {
    return Object.assign(Object.create(this), this.globalCtx, ...ctx);
  }
  use(key: string, plugin: Plugin): Dispatcher {
    if (this.plugins.has(key)) {
      throw new Error(`Plugin ${key} added twice`);
    }
    this.plugins.set(key, plugin);
    if (plugin.initialize) plugin.initialize(this.buildCtx());
    return this;
  }
  unuse(key: string): Dispatcher {
    this.plugins.delete(key);
    return this;
  }
  do(cmdArg: string | CommandObject, ...args: unknown[]): unknown {
    let callCtx: CommandObject | void;
    let pluginCmd: string;
    if (typeof cmdArg === "string") {
      pluginCmd = cmdArg;
    } else {
      callCtx = cmdArg;
      pluginCmd = cmdArg.$cmd;
    }
    const cmdCtx = this.cmdCtx.get(pluginCmd);
    const ctx = this.buildCtx(cmdCtx, callCtx);
    let pluginKey: string;
    let cmd: string;
    const match = /^([^ \/]+)\/?(.*)$/.exec(pluginCmd);
    if (match) {
      pluginKey = match[1];
      cmd = match[2];
    } else {
      throw new Error(`Unable to parse "${pluginCmd}"`);
    }
    const plugin = this.plugins.get(pluginKey);
    if (!plugin) {
      throw new Error(`Unknown plugin "${pluginKey}"`);
    }
    let result: unknown;
    try {
      if (typeof plugin[cmd] === "function") {
        result = plugin[cmd](ctx, ...args);
      } else if (typeof plugin.defaultHandler === "function") {
        result = plugin.defaultHandler(ctx, cmd, ...args);
      } else {
        throw new Error(`No handler for ${pluginCmd}`);
      }
    } catch (err) {
      return this._handleCommandError(err, pluginCmd, args, ctx);
    }
    if (result && result instanceof Promise) {
      return result.catch((err) =>
        this._handleCommandError(err, pluginCmd, args, ctx)
      );
    }
    return result;
  }
  emit(topic: string, ...args: unknown[]): Dispatcher {
    super.emit(topic, this.buildCtx(), ...args);
    return this;
  }
  private _handleCommandError(
    err: Error | CommandError,
    cmd: string,
    args: unknown[],
    ctx: CommandContext,
  ): void {
    let cmdErr = err instanceof CommandError ? err : new CommandError(err);
    cmdErr.commandStack.push({ cmd, args: ctx.$logArguments ? args : [] });
    if (ctx.$catch) {
      this.emit(ctx.$catch, cmdErr);
      return;
    }
    throw cmdErr;
  }
}
