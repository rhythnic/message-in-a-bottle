import { FirstHandlerArg, CommandStackItem, CommandError } from "../mod.ts";
import { EventEmitter } from "../deps.ts";

const LEVELS = ["log", "info", "warn", "error"];

interface LogData {
  time: string;
  level?: string;
  message?: string;
  stack?: string;
  commandStack?: CommandStackItem[];
}

interface LoggerOptions {
  includeStack?: boolean;
  level?: string;
  print?: boolean;
}

interface DataArg {
  level?: string;
  [propName: string]: unknown;
}

type LoggerArg = DataArg | Error | CommandError;
type ErrorArg = Error | CommandError | null;

export class Logger extends EventEmitter {
  opts: LoggerOptions;
  constructor(opts: LoggerOptions = {}) {
    super();
    this.opts = Object.assign({ print: true, level: "info" }, opts);
  }
  defaultHandler(
    $: FirstHandlerArg,
    cmd: string,
    arg1: LoggerArg,
    arg2?: LoggerArg,
  ) {
    return this.log($, arg1, arg2);
  }
  log($: FirstHandlerArg, arg1: LoggerArg, arg2?: LoggerArg): LogData {
    let errArg: ErrorArg = arg2 instanceof Error
      ? arg2
      : arg1 instanceof Error
      ? arg1
      : null;
    const data: LogData = Object.assign(
      { time: new Date().toISOString() },
      arg1,
      arg2,
    );
    if (errArg instanceof Error) {
      data.message = errArg.message;
      if (this.opts.includeStack) data.stack = errArg.stack;
    }
    if (errArg instanceof CommandError) {
      data.commandStack = errArg.commandStack;
    }
    data.level = data.level || (errArg ? "error" : "log");
    if (
      this.opts.print !== false &&
      $.print !== false &&
      LEVELS.indexOf(data.level) >= LEVELS.indexOf(this.opts.level as string)
    ) {
      this.print(data);
    }
    this.emit("log", $, data);
    return data;
  }
  print(data: LogData): void {
    if (typeof data.level !== "string" || LEVELS.indexOf(data.level) < 0) {
      throw new Error(
        `Logger can't print data with invalid level ${data.level}`,
      );
    }
    if (!data.commandStack) {
      this._console(data.level, "%o", data);
    } else {
      let { commandStack, ...rest } = data;
      this._console(data.level, "%o", rest);
      console.group();
      this._console(data.level, "Command Stack:");
      for (let i = 0; i < commandStack.length; i++) {
        this._console(
          data.level,
          "%s %O",
          commandStack[i].cmd,
          commandStack[i].args,
        );
      }
      console.groupEnd();
    }
  }
  private _console(level: string, ...args: unknown[]): void {
    switch (level) {
      case "error":
        console.error(...args);
        break;
      case "warn":
        console.warn(...args);
        break;
      case "info":
        console.info(...args);
        break;
      default:
        console.log(...args);
    }
  }
}
