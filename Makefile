runtest:
	- docker build -f ./build/Dockerfile.test -t mibtest:latest .
	- docker create --name mibtest mibtest:latest
	- docker run mibtest lint --unstable
	- docker run mibtest
	- docker rm mibtest
transpile:
	- docker build -f ./build/Dockerfile.npm -t mibnpm:latest .
	- docker create --name mibnpm mibnpm:latest
	- docker cp mibnpm:/app/lib ./lib
	- docker rm mibnpm
clean:
	- docker image rm mibtest:latest mibnpm:latest