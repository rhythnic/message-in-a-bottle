import { MessageTransport } from "../src/message-transport/mod.ts";
import { Dispatcher } from "../src/mod.ts";
import { assertMatch, assertEquals, assertStrictEquals } from "./deps.ts";
import { spy } from "./test-utils.js";

const ARG1 = "ARG1";
const ARG2 = {};
const RESULT = "RESULT";
const ERROR = new Error("TEST");

function setup({ response } = {}) {
  const dispatcherA = new Dispatcher();
  const dispatcherB = new Dispatcher();
  const mtA = new MessageTransport();
  const mtB = new MessageTransport();

  const B = {
    cmd: spy(() => response),
  };

  dispatcherA.use("worker", mtA);
  dispatcherB.use("b", B).use("main", mtB);
  return { dispatcherA, dispatcherB, B, mtA, mtB };
}

function connect(mtA, mtB) {
  mtB.on("send", (msg) => mtA.execute(msg));
  mtA.on("send", (msg) => mtB.execute(msg));
}

Deno.test("successful command", async () => {
  const { dispatcherA, B, mtA, mtB } = setup({
    response: Promise.resolve(RESULT),
  });
  connect(mtA, mtB);
  const result = await dispatcherA.do("worker/b/cmd", ARG1, ARG2);
  assertStrictEquals(result, RESULT);
  const args = B.cmd.calls[0];
  assertEquals(args.slice(1), [ARG1, ARG2]);
});
Deno.test("failed command", () => {
  const { dispatcherA, mtA, mtB } = setup({ response: Promise.reject(ERROR) });
  connect(mtA, mtB);
  return dispatcherA.do("worker/b/cmd", ARG1, ARG2).catch((err) => {
    assertStrictEquals(err.message, ERROR.message);
  });
});
Deno.test("forward event", () => {
  return new Promise((resolve, reject) => {
    try {
      const { dispatcherA, dispatcherB, mtA, mtB } = setup();
      connect(mtA, mtB);
      dispatcherA.on("test", (_, ...args) => {
        mtA.sendEvent("test", ...args);
      });
      dispatcherB.on("test", (...args) => {
        assertStrictEquals(typeof args[0].do, "function");
        assertEquals(args.slice(1), [ARG1, ARG2]);
        resolve();
      });
      dispatcherA.emit("test", ARG1, ARG2);
    } catch (error) {
      reject(error);
    }
  });
});
Deno.test("vet and respond", async () => {
  const { dispatcherA, mtA, mtB, dispatcherB } = setup({
    response: Promise.resolve(RESULT),
  });
  mtA.on("send", (msg) => {
    const req = mtB.receive(msg);
    if (req && req.type === "command") {
      mtB.respond(req, dispatcherB.do(req.key, ...req.args));
    }
  });
  mtB.on("send", (msg) => mtA.execute(msg));
  const result = await dispatcherA.do("worker/b/cmd", ARG1, ARG2);
  assertStrictEquals(result, RESULT);
});
Deno.test("request can timeout", async () => {
  const dispatcherA = new Dispatcher();
  const dispatcherB = new Dispatcher();
  const mtA = new MessageTransport({ timeout: 100 });
  const mtB = new MessageTransport();

  const B = {
    cmd: () => new Promise((rs) => setTimeout(rs, 150)),
  };

  dispatcherA.use("worker", mtA);
  dispatcherB.use("b", B).use("main", mtB);

  return dispatcherA
    .do("worker/b/cmd")
    .then(() => {
      throw new Error("Request never timed out");
    })
    .catch((err) => {
      assertMatch(err.message, /timed out/);
    });
});
