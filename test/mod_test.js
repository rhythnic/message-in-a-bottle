import { Dispatcher } from "../src/mod.ts";
import { assertMatch, assertEquals, assertStrictEquals } from "./deps.ts";
import { spy } from "./test-utils.js";

const ARG1 = "ARG1";
const ARG2 = {};
const RESULT = "RESULT";
const ERROR = () => new Error("TEST");

function setup(cmdResponse) {
  const globalContext = { foo: {} };
  const cmdContext = {};
  const dispatcher = new Dispatcher();
  dispatcher.mergeContext(globalContext);
  const A = {
    initialize: spy(($) => {
      $.mergeContext({ bar: cmdContext }, "a/cmd");
    }),
    cmd: spy(($, ...args) => $.do("b/cmd", ...args)),
  };
  const B = {
    defaultHandler: spy(($, cmd, ...args) => B._cmd($, ...args)),
    _cmd: spy(() => cmdResponse),
  };
  dispatcher.use("a", A);
  dispatcher.use("b", B);

  return {
    dispatcher,
    A,
    B,
    globalContext,
    cmdContext,
  };
}

Deno.test("initializes plugins", () => {
  const { A } = setup();
  const { calls } = A.initialize;
  assertStrictEquals(typeof calls[0][0].do, "function");
});
Deno.test("successful command", async () => {
  const { dispatcher, A, B } = setup(Promise.resolve(RESULT));
  const result = await dispatcher.do("a/cmd", ARG1, ARG2);
  assertStrictEquals(result, RESULT);
  let handlerArgs = A.cmd.calls[0];
  assertStrictEquals(handlerArgs[1], ARG1);
  assertStrictEquals(handlerArgs[2], ARG2);
  handlerArgs = B.defaultHandler.calls[0];
  assertStrictEquals(typeof handlerArgs[0].do, "function");
  assertStrictEquals(handlerArgs[1], "cmd");
  assertStrictEquals(handlerArgs[2], ARG1);
  assertStrictEquals(handlerArgs[3], ARG2);
});
Deno.test("failed command", () => {
  const { dispatcher } = setup(Promise.reject(ERROR()));
  return dispatcher
    .do("a/cmd", ARG1, ARG2)
    .then(() => {
      throw new Error("Command should have thrown");
    })
    .catch((err) => {
      assertStrictEquals(err.toString(), ERROR().toString());
    });
});
Deno.test("emit event", () => {
  return new Promise((resolve, reject) => {
    try {
      const eventListener = ($, ...args) => {
        assertStrictEquals(typeof $, "object");
        assertEquals(args, [ARG1, ARG2]);
        resolve();
      };
      const { dispatcher } = setup();
      dispatcher.on("event", eventListener);
      dispatcher.emit("event", ARG1, ARG2);
    } catch (err) {
      reject(err);
    }
  });
});
Deno.test("context injection", () => {
  const { dispatcher, A, globalContext, cmdContext } = setup(RESULT);
  const baz = {};
  dispatcher.do({ $cmd: "a/cmd", baz }, ARG1, ARG2);
  const ctx = A.cmd.calls[0][0];
  assertStrictEquals(ctx.foo, globalContext.foo);
  assertStrictEquals(ctx.bar, cmdContext);
  assertStrictEquals(ctx.baz, baz);
  assertStrictEquals(typeof ctx.do, "function");
});
Deno.test("$catch option", () => {
  return new Promise((resolve, reject) => {
    try {
      const { dispatcher } = setup(Promise.reject(ERROR()));
      dispatcher.on("cmd-error", (_, err) => {
        assertStrictEquals(err.toString(), ERROR().toString());
        resolve();
      });
      dispatcher.do({ $cmd: "a/cmd", $catch: "cmd-error" }, ARG1, ARG2);
    } catch (err) {
      reject(err);
    }
  });
});
Deno.test("$logArguments option true", () => {
  const { dispatcher } = setup(Promise.reject(ERROR()));
  dispatcher.mergeContext({ $logArguments: true });
  return dispatcher.do("a/cmd", ARG1, ARG2).catch((err) => {
    assertStrictEquals(err.toString(), ERROR().toString());
    const { commandStack } = err;
    assertStrictEquals(commandStack[0].cmd, "b/cmd");
    assertEquals(commandStack[0].args, [ARG1, ARG2]);
    assertStrictEquals(commandStack[1].cmd, "a/cmd");
    assertEquals(commandStack[1].args, [ARG1, ARG2]);
  });
});
Deno.test("$logArguments option false", () => {
  const { dispatcher } = setup(Promise.reject(ERROR()));
  dispatcher.mergeContext({ $logArguments: true });
  return dispatcher
    .do({ $cmd: "a/cmd", $logArguments: false }, ARG1, ARG2)
    .catch((err) => {
      assertStrictEquals(err.toString(), ERROR().toString());
      const { commandStack } = err;
      assertStrictEquals(commandStack[0].cmd, "b/cmd");
      assertEquals(commandStack[0].args, [ARG1, ARG2]);
      assertStrictEquals(commandStack[1].cmd, "a/cmd");
      assertEquals(commandStack[1].args, []);
    });
});
Deno.test("removes plugin", () => {
  const { dispatcher } = setup();
  dispatcher.unuse("b");
  try {
    dispatcher.do("a/cmd", ARG1, ARG2);
  } catch (err) {
    assertMatch(err.message, /Unknown plugin "b"/);
  }
});
