import { Logger } from "../src/logger/mod.ts";
import { Dispatcher, CommandError } from "../src/mod.ts";
import { assertEquals, assertStrictEquals } from "./deps.ts";
import { spy } from "./test-utils.js";

const ERROR = () => new Error("TEST");

function setup({ level = "debug", includeStack } = {}) {
  const dispatcher = new Dispatcher();
  const logger = new Logger({ level, includeStack });
  const listener = spy();
  logger.on("log", listener);
  dispatcher.use("log", logger);
  return { dispatcher, listener };
}

function mockConsole(level) {
  let prev = console[level];
  console[level] = spy();
  return () => {
    console[level] = prev;
  };
}

Deno.test("returns a log document", () => {
  const { dispatcher } = setup();
  const doc = dispatcher.do("log", { level: "info", note: "TEST" });
  assertStrictEquals(typeof doc.time, "string");
  assertStrictEquals(doc.level, "info");
  assertStrictEquals(doc.note, "TEST");
});
Deno.test("merges object arguments", () => {
  const { dispatcher } = setup();
  const doc = dispatcher.do("log", { level: "info" }, { note: "TEST" });
  assertStrictEquals(doc.level, "info");
  assertStrictEquals(doc.note, "TEST");
});
Deno.test("log to console at info level", () => {
  let unmockConsole = mockConsole("info");
  const { dispatcher } = setup();
  dispatcher.do("log", { level: "info", note: "TEST" });
  const args = console.info.calls[0];
  assertStrictEquals(args[1].level, "info");
  assertStrictEquals(args[1].note, "TEST");
  unmockConsole();
});
Deno.test("log to console at error level", () => {
  let unmockConsole = mockConsole("error");
  const { dispatcher } = setup();
  dispatcher.do("log", { level: "error", note: "TEST" });
  const args = console.error.calls[0];
  assertStrictEquals(args[1].level, "error");
  assertStrictEquals(args[1].note, "TEST");
  unmockConsole();
});
Deno.test("calls the listen option with the log record", () => {
  const { dispatcher, listener } = setup();
  const doc = dispatcher.do("log", { level: "info", note: "TEST" });
  assertStrictEquals(listener.calls[0][1], doc);
});
Deno.test("copies message and optionally stack to the doc", () => {
  let _ = setup();
  let doc = _.dispatcher.do("log", null, ERROR());
  assertStrictEquals(typeof doc.stack, "undefined");
  assertStrictEquals(doc.message, ERROR().message);
  _ = setup({ includeStack: true });
  doc = _.dispatcher.do("log", null, ERROR());
  console.log(doc);
  assertStrictEquals(typeof doc.stack, "string");
});
Deno.test("accepts Error as the first argument", () => {
  const { dispatcher } = setup();
  let doc = dispatcher.do("log", ERROR());
  assertStrictEquals(doc.message, ERROR().message);
});
Deno.test("defaults doc.level to error", () => {
  const { dispatcher } = setup();
  let doc = dispatcher.do("log", { level: "warn" }, ERROR());
  assertStrictEquals(doc.level, "warn");
  doc = dispatcher.do("log", null, ERROR());
  assertStrictEquals(doc.level, "error");
});
Deno.test("adds the commandStack to the returned doc", () => {
  const { dispatcher } = setup();
  const error = new CommandError("TEST");
  error.commandStack.push({ cmd: "foo", args: [0] });
  const doc = dispatcher.do("log", error);
  assertEquals(doc.commandStack, error.commandStack);
});
