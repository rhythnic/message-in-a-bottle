# Message in a Bottle

Message in a Bottle, or MIB, is a plugin-based command dispatcher and event emitter. MIB dispatches commands by delegating them to plugins. [This blog post][dispatcherpatternpost] gives a good overview of the command dispatcher pattern.

**Goals**

- A pattern for business logic with inversion of control and dependency injection
- Easy communication between machines or processes
- Reusable code (plugins)
- Centralized error handling
- Easy debugging

## Deno import

- **latest** - `import { Dispatcher } from "https://gitlab.com/rhythnic/message-in-a-bottle/-/raw/master/src/mod.ts"`
- **versioned** - `import { Dispatcher } from "https://gitlab.com/rhythnic/message-in-a-bottle/-/raw/VERSION/src/mod.ts"`

## NPM install

`npm install message-in-a-bottle@next`

## Usage

```
import { Dispatcher } from 'message-in-a-bottle'

class UserRole {
  load($, id) {
    return $.db.users.get(id)
  }
}

const todoRole = {
  async create($, todo) {
    await $.do('user/load', todo.author).catch(err => {
      throw new Error(`Invalid author ${todo.author}`)
    })
    await $.db.todos.put({ ...todo, id: new Date().toISOString() })
  }
}

const dispatcher = new Dispatcher()
dispatcher.mergeContext({ db: myDbClient })

dispatcher.use('user', new UserRole())
dispatcher.use('todo', todoRole)

dispatcher.on('error', err => {
  console.error(err)
})

const todo = {
  status: 'INCOMPLETE',
  text: 'Send invitations',
  author: userId
}

dispatcher.do({ $command: 'todo/create', $catch: 'error' }, todo)
```

### Review

In the above code, there are 2 plugins, user and todo. Plugins can be for local code, like above,
or they can be a transport plugin to send command and events to other machines/processes.
This design is heavily influenced by [Seneca][seneca], but MIB can be used in a server or in the browser.

Context/dependencies can be added that are available globally, by calling the `mergeContext` command. The first
argument of each command handler and event listener is a context object that has all added dependencies.
The context object's prototype is the dispatcher, so all dispatcher methods are available on that object too.

Commands above are called in 2 different ways. Normally, you'll use the form `$.do('user/load', todo.author)`.
If you need to pass context or options, you can pass an object instead,
such as `$.do({ $command: 'todo/create', $catch: 'error' }, todo)`. This example uses the `$catch` option,
which catches any error and emits an event. The string "error" indicates that an "error" event
should be emitted.

## Dispatcher API

[Dispatcher API Documentation][dispatcher]

## Plugins

Plugins are objects that implement a simple interface so that the dispatcher can interact with it.
If there is an `initialize` method, it's called when the plugin is added.
Plugins can also have a `defaultHandler` method which handles all commands for which the plugin
doesn't have a method matching the command string. Plugins are assigned a key. A command `user/load`
will look for a `user` plugin and call the `load` method on it. If there is no `load` method, the dispatcher
will call the `defaultHandler` method.

### Included plugins

Message in a Bottle ships with these plugins.

- [Logger][logger] - For logging
- [MessageTransport][messagetransport] - For connecting dispatchers over a message protocol such as web sockets

## LICENSE

MIT license, Copyright Nicholas Baroni 2020

[dispatcher]: ./docs/dispatcher.md
[messagetransport]: ./docs/message-transport.md
[logger]: ./docs/logger.md
[seneca]: https://senecajs.org/
[dispatcherpatternpost]: https://olvlvl.com/2018-04-command-dispatcher-pattern
