# Logger

The logger plugin provides basic logging that works with the `commandStack` property
on errors. `Logger` extends the `heehaw` [EventEmitter][heehaw].

## Usage

```
const { Dispatcher } = require('message-in-a-bottle');
const { Logger } = require('message-in-a-bottle/logger');

const dispatcher = new Dispatcher();
dispatcher.setContext({ db: myDbClient });
const logger = new Logger();
dispatcher.use('log', logger);

logger.on('log', ($, data) => {
  $.db.put(`logs/${new Date().toISOString()}`, data);
})

dispatcher.on('error', err => dispatcher.do('log', err))

const value = 5;
dispatcher.do('log', { level: 'info', note: 'Some value', value });

```

## Logger API

### constructor([options])

```
interface LoggerOptions {
  includeStack?: boolean; // put the error stack on the log document
  level?: string; // logs below this level will be ignored
  print?: boolean; // print the log to the console
}
```

### dispatcher.do('log', arg1[, arg2])

`log` merges/normalizes `arg1` and `arg2` into one log record. If either argument is an instance of Error, `message`,
`commandStack`, and optionally `stack` are copied into the log record and the level is set to "error".

```
try {
  let result = $.do('role/cmd', ARG)
  $.do('log', { level: 'log', note: 'role/cmd result', result })
} catch (err) {
  $.do('log', { note: 'role/cmd failed' }, err)
}
```

### The "log" event

Whenever a log occurs, the logger will emit a "log" event.

```
logger.on('log', ($, data) => {
  $.db.put(`logs/${new Date().toISOString()}`, data);
})
```
