# HTTP Examples

There's no official HTTP plugin, but this document serves as a refernce to how HTTP integration might happen.

## Example using window.fetch

This example uses [Fetch][fetch] in the client and [Express][express] in the server.

**client/fetch-json.js**

```
function fetch (...args) {
	const res = await window.fetch(...args)
  if (res.ok) return res
  const err = new Error(res.statusText)
  err.statusCode = err.status = res.status
  throw err
}

const jsonHeaders = { accept: 'application/json', 'content-type': 'application/json' }

export async function fetchJson (url, init = {}) {
  init.headers = init.headers ? Object.assign({}, init.headers, jsonHeaders) : jsonHeaders
  if (init.body && typeof init.body !== 'string') init.body = JSON.stringify(init.body)
	return fetch(url, init).then(x => x.json())
}
```

**client/HttpTransport.js**

```
import { fetchJson } from './fetch-json.js'

class HttpTransport {
	constructor({ baseUrl }) {
		this.baseUrl = baseUrl
	}
	initialize($) {
		this.$ = $
	}
	async defaultHandler($, cmd, ...args) {
		const result = await fetchJson({
			body; args,
			method: 'POST',
			url: `${baseUrl}/commands/${cmd}`
		})
	}
	sendEvent(topic, ...args) {
		fetchJson({
			body; args,
			method: 'POST',
			url: `${baseUrl}/events/${topic}`
		})
		.catch(err => {
			this.$.emit('error', new Error(`Failed to send event ${topic}: ${err.message}`))
		})
	}
}
```

**client/index.js**

```
import { Dispatcher } from 'message-in-a-bottle
import { HttpTransport } from './HttpTransport.js'

const dispatcher = new Dispatcher();
const serverClient = new HttpTransport()

dispatcher.use('server', serverClient)

dispatcher.on('error', (_, err) =>
  serverClient.sendEvent('client-error', err.toString())
)

// dispatch some command to server
// dispatcher.do('server/auth/login', credentials)
```

**server**

```
app.post('/api/mib/commands/:cmd', authorize, async function (req, res, next) {
  dispatcher.do(req.params.cmd, ...req.body).then(res.json.bind(res), next)
})
app.post('/api/mib/events/:topic', authorize, async function (req, res) {
  dispatcher.emit(req.params.topic, ...req.body)
  res.json('')
})
```

### Review

This HTTP plugin example would depend on 2 endpoints in the server, one for events and one for commands.
It's listening only for error events and will emit them to the back-end as `client-error`. You could
prefix any command with `server/` to send it to the server. If the `server/auth/login` command is dispatched
from the client, it would be dispatched on the server as `auth/login`, and delegated to the `auth` plugin.

[fetch]: https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
[express]: http://expressjs.com/
