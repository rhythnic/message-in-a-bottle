# Dispatcher

`Dispatcher` extends the `heehaw` [EventEmitter][heehaw]. Event listeners will be called with the
dispatcher's context object as the first argument.

## Deno import

- **latest** - `import { Dispatcher } from "https://gitlab.com/rhythnic/message-in-a-bottle/-/raw/master/src/mod.ts"`
- **versioned** - `import { Dispatcher } from "https://gitlab.com/rhythnic/message-in-a-bottle/-/raw/VERSION/src/mod.ts"`

## Node import

`import { Dispatcher } from 'message-in-a-bottle`

## Dispatcher API

### Constructor

```
const dispatcher = new Dispatcher()
```

### mergeContext(context: CommandContext, command?: string): Dispatcher

If the command is included, the context is merged into the context for that command, otherwise, the context is merged into the global context.
Context holds dependencies that are available in the command handlers and event listeners. The prototype of the context object is the dispatcher,
so dispatcher methods are available on the context object. Context can also hold options that alter the behavior of the dispatcher.
Context can also be passed when dispatching a command.

**options**

- \$catch [String][string] - When this command handler throws an error, catch the error and emit the
  event indicated by the string value of the `$catch` option. This is usually only used when dispatching a command,
  and not used with `mergeContext`.
- \$logArguments [Boolean][boolean] - Attach the invocation arguments to the error.

```
dispatcher.mergeContext({ $logArguments: true, db: myDbClient })
dispatcher.mergeContext({ $logArguments: false }, 'user/create')
```

### use(key: string, plugin: Plugin): Dispatcher

Add a plugin

```
dispatcher.use('myPlugin', myPlugin)
```

### unuse(key: string): Dispatcher

Remove a plugin

```
dispatcher.unuse('myPlugin')
```

### do(cmdArg: string | CommandObject, ...args: unknown[]): unknown

The cmdArg string uses a forward slash `/` to separate the plugin key and the command.
If you want to add context to the call, pass an object with a `$command` string.

```
dispatcher.do('myPlugin/cmd', ...args)
dispatcher.do({ $command: 'myPlugin/cmd', $catch: 'error' }, ...args)
```

## Context Object

The context object is passed as the first argument to any function called by the dispatcher,
including event listeners and all plugin methods. The prototype of the context object is the
dispatcher, so all dispatcher methods are available on the context. Context is always merged,
with more specific context overriding more global context. Essentially, it acts like this:

`Object.assign(Object.create(dispatcher), globalContext, commandContext, callTimeContext)`

## Plugins

A plugin implements an interface that defines 2 optional methods.

### initialize(context: CommandContext & Dispatcher): void

If present, initialize is called when the plugin is added.

### defaultHandler(context: CommandContext & Dispatcher, cmd: string, ...args: unknown[]): unknown

The default handler is called if the plugin doesn't have a method matching the command.

## Error Handling

The dispatcher appends an [Array][array] `commandStack` to errors thrown, which contains a history of the commands and arguments that led to the error. The [logger][logger] plugin uses `commandStack`. By default command arguments aren't logged. If the option `$logArguments` is true in the command's context, it will put the arguments into `commandStack`.

Any option can be set globally by using `dispatcher.mergeContext`, so you can do `dispatcher.mergeContext({ $logArguments: true })` to log arguments by default, and then when arguments shouldn't be logged, override that setting per command, with `dispatcher.mergeContext({ $logArguments: false }, 'plugin/command')`.

```
class UserRole {
  initialize(ctx) {
    ctx.mergeContext({ $logArguments: false }, 'user/register')
  }
  register(ctx, { username, password }) {}
}

dispatcher.use('user', new UserRole())
dispatcher.mergeContext({ $logArguments: true })
```

[object]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object
[string]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String
[function]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function
[boolean]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean
[array]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
[heehaw]: https://gitlab.com/rhythnic/heehaw
