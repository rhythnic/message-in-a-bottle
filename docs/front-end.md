# Front-end development

`message-in-a-bottle` works best as the central piece in a framework-less
front-end application.

For example, these libraries could be used with `message-in-a-bottle` on the front-end.

- `lit-html` for rendering the html
- `loud-map` for state management
- `parcel` to bundle the application

## Counter Example

**index.html**

```
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="./index.css" rel="stylesheet">
	<title>App Title</title>
</head>
<body>
	<noscript>
		<strong>App requires JavaScript. Please enable it to continue.</strong>
	</noscript>
	<div id="root"></div>
	<script src="./index.js"></script>
</body>
</html>
```

**index.css**

```
:root {
  --color-primary: 'rgb(255,20,147)'
}
```

**index.js**

```
import { Dispatcher } from 'message-in-a-bottle'
import { state } from './state'
import { App } from './roles/app

export const dispatcher = new Dispatcher();
dispatcher.setContext({ state });
dispatcher.use('app', new App())
```

**state.js**

```
import { LoudMap } from 'loud-map'

export const state = new LoudMap()
```

**roles/app.js**

```
import { render as renderView } from 'lit-html'
import counterView from '../views/counter.js`

export class App {
  initialize($) {
    $.setContext({ rootDiv: document.querySelector('#root'), render: renderView }, 'render')

    const emitError = err => $.emitt('error', err)
	  const render = () => $.do({ $command: 'app/render', $catch: 'error' })

    $.state.batch({ count: 0 })
    $.state.compute('doubleCount', ['count'], x => x * 2)
    $.state.events.on('change', render)

    $.on('error', err => $.do('app/handleError', err))
    $.on('warn', console.warn.bind(console))

    window.onunhandledrejection = emitError
    window.addEventListener('error', emitError, /*useCapture*/true)
    window.addEventListener('load', render)
  }
  handleError (_, err) {
    const json = JSON.stringify(err, Object.getOwnPropertyNames(err))
    console.error(json)
  }
  incrementCount ($) {
    $.state.set('count', $.state.get('count') + 1)
  }
  render ($) {
    return $.render(counterView($)), $.rootDiv)
  }
}
```

**views/counter.js**

```
import { html } from 'lit-html'

export default function counterView ($) {
  return html`
  <div class="counterView">
    <p>Count: ${$.state.get('count')}</p>
    <p>Count * 2: ${$.state.get('doubleCount')}
    <button @click=${() => $.do('app/incrementCounter')}>
      Increment
    </button>
  </div>
  `
}
```
