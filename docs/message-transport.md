# MessageTransport

The `MessageTransport` plugin contains the core logic for sending commands and events
between two dispatchers as messages. You can use this with web sockets, web workers,
and other direct messaging protocols. `MessageTransport` extends the `heehaw` [EventEmitter][heehaw].

## Import

### Deno

- **latest** - `import { MessageTransport } from "https://gitlab.com/rhythnic/message-in-a-bottle/-/raw/master/src/message-transport/mod.ts"`
- **versioned** - `import { MessageTransport } from "https://gitlab.com/rhythnic/message-in-a-bottle/-/raw/VERSION/src/message-transport/mod.ts"`

### Node

`import { MessageTransport } from 'message-in-a-bottle/message-transport`

## Web Socket Example

This is based on the [client authentication example][wsclientauth] from the `ws` package.

**server**

```
const WebSocket = require('ws');
const { Dispatcher } = require('message-in-a-bottle');
const { MessageTransport } = require('message-in-a-bottle/message-transport');

const dispatcher = new Dispatcher();
const wss = new WebSocket.Server({ port: 8080 });
const messageTransports = new WeakMap()

function broadcastEvent (_, topic, ...args) {
  wss.clients.forEach((ws) => {
    let mt = messageTransports.get(ws)
    if (mt) mt.sendEvent(topic, ...args)
  })
}

dispatcher.on('a-broadcasted-event', broadcastEvent);

wss.on('connection', function connection(ws, request, client) {
  const mt = new MessageTransport();
  messageTransports.set(ws, mt);
  dispatcher.use(client, mt);
  mt.on('send', msg => ws.send(JSON.stringify(msg)))
  ws.on('message', function message(msg) {
    const request = mt.receive(JSON.parse(msg))
    if (!request) return
    if (request.type === "event") {
      // if user is authorized to emit this event
      dispatcher.emit(req.key, ...req.args);
      return;
    }
    // if user is authorized to dispatch this command
    try {
      const result = dispatcher.do({ $command: request.key, client }, ...request.args);
      mt.respond(request, result);
    } catch (err) {
      mt.respond(request, err);
    }
  });
  ws.on('close', () => {
    clients.delete(client);
    dispatcher.unuse(client);
  })
});

```

**client**

```
import { Dispatcher } from 'message-in-a-bottle'
const { MessageTransport } = require('message-in-a-bottle/message-transport');

const webSocket = new WebSocket(url, protocols)
const dispatcher = new Dispatcher()
const socketMt = new MessageTransport({ timeout: 3000 })

socketMt.on('send', msg => webSocket.send(JSON.stringify(msg)))
webSocket.onmessage = ({ data }) => {
  socketMt.execute(JSON.parse(data))
}

dispatcher.use('wsserver', socketMt)

webSocket.onclose = () => {
  dispatcher.unuse('wsserver')
  dispatcher.emit('web-socket-closed')
}
```

### Review

The MessageTransport API is designed to give you control over what events and commands are executed. In the example, the server uses the long version,
which allows it to control which events or commands can be disptached by the client. The client is using
the `execute` method, which makes things much shorter, but implies trust in the incoming events and commands.

This setup would allow a server to exchange events and commands with multiple clients.

## Web Worker Example

**main browser process**

```

const dispatcher = new Dispatcher()
const workerMt = MessageTransport()
dispatcher.use('worker', workerMt)
const worker = new Worker('worker.js')
workerMt.on('send', msg => worker.postMessage(msg))
worker.onmessage = ({ data }) => workerMt.execute(data)

```

**worker process**

```

const dispatcher = new Dispatcher()
const mainMt = MessageTransport()
dispatcher.use('main', mainMt)
mainMt.on('send', msg => postMessage(msg))
onmessage = ({ data }) => mainMt.execute(data)

```

## MessageTransport API

## new MessageTransport(opts: MessageTransportOptions): MessageTransport

```
interface MessageTransportOptions {
  timeout?: number;
}
```

- timeout defaults to 5000 if not provided

### The "send" event

A MessageTransport emits a "send" event when it has a message that should be sent over the wire.
The message is an array that adheres to a simple protocol understood by the MessageTransport plugin.
All arguments for events and commands that are sent should be serializable.

```
mt.on("send", msg => worker.postMessage(msg))
mt.on("send", msg => ws.send(JSON.stringify(msg)))
```

### sendEvent(topic: string, ...args: unknown[]): void

Use this to forward an event emitted by the dispatcher.

```
dispatcher.on('some-event', (_, topic, ...args) => mt.sendEvent(topic, ...args))
```

### receive(data: Message): null | Request

The return value will be null if the message is a response to a request that we sent earlier.
If the message is to request that we emit and event or dispatch a command, the Request object
contains the information needed to complete that request.

#### Request Interface

```
interface Request {
  type: string; // "event" or "command"
  correlationId?: string; // only present if type is "command"
  key: string; // command string or event topic
  args: unknown[];
}
```

```
const request = mt.receive(JSON.parse(msg))
if (request) {
  ...
}
```

### respond(req: Request, result: unknown): void

Use this to send back the result of a command. The result can be any serializable value, including an Error.
The result can also be a promise that resolves to any serializable value or rejects with an Error.

```
try {
  mt.respond(request, dispatcher.do(request.key, ...request.args));
} catch (err) {
  mt.respond(request, err);
}
```

### execute(msg: Message): void

Use this to execute a message without checking it first.

```
worker.onmessage = ({ data }) => workerMt.execute(data)
```

[object]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object
[function]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function
[heehaw]: https://gitlab.com/rhythnic/heehaw
[wsclientauth]: https://github.com/websockets/ws#client-authentication

```

```
